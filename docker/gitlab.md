## Build the Docker image
docker build -t <image_name> .

## Create a tag
docker tag b18d620c0d78 registry.gitlab.com/derpiet/<repo>/apache_web:v1

## Commit the image
docker commit -m "Commit message" <image_id> registry.gitlab.com/derpiet/<repo>/apache_web

## Pull an image
docker pull registry.gitlab.com/derpiet/<repo>/apache_web:v1
