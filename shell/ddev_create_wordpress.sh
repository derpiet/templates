#!/bin/bash

set -e;

if [ $# -lt 1 ]; then 
    echo "usage: $0 <directory>";
    exit 1;
fi

DESTINATION=$1;
PROJECT_NAME=$(basename $DESTINATION)

mkdir $DESTINATION

# Work in the new directory
cd $DESTINATION;

# Optional if you have too many SSH keys in your .ssh directory.
# Then create a .ssh/ddev directory and place there only the key you need.
# [[ -d ~/.ssh/ddev ]] && ddev auth ssh -d ~/.ssh/ddev

ddev config --project-name ${PROJECT_NAME} --project-type wordpress

ddev start
ddev exec wp core download

HTTPS_URL=$(ddev describe --json-output | jq '.raw.httpsurl')
WP_CLI="ddev exec php -d error_reporting=E_ERROR /usr/local/bin/wp"

# Automatic installation
$WP_CLI core install \
    --url="$HTTPS_URL" \
    --title="$PROJECT_NAME" \
    --admin_user=admin \
    --admin_password=admin \
    --admin_email="admin@example.com"

$WP_CLI plugin update --all
$WP_CLI --info

echo ""
echo ""
echo "---------------------------------------------------------------------------"
echo "READY! Please change directory to \"$DESTINATION\"                         "
echo "---------------------------------------------------------------------------"
echo ""
echo "Username: admin"
echo "Password: admin"
echo ""
